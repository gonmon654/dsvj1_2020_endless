#include "Player.h"

namespace endless
{
	namespace player
	{
		int playerHeight = (int)((screen::screenWidth / 20) * 1.5);
		int playerWidth = (int)((screen::screenHeight / 20) * 1.5);
		const int animFrames = 30;

		int go_up = KEY_SPACE;
		int jump = KEY_W;
		int duck = KEY_S;
		int phase = KEY_P;
		int flame = KEY_F;

		Player::Player()
		{
			init();
		}

		void Player::init()
		{
			_lives = 3;
			_current = state::middleLane;
			_phasing = false;
			_flaming = false;
			_counter = 0;
			_seconds = 0;
			_gameover = false;
			hitbox = { (float)((screen::screenWidth / 8) * 1), (float)(screen::middleLane.y - playerHeight),(float)playerWidth,(float)playerHeight };
			_score = 0;
			sourceRec = { 0,0,(float)(textures::player.width / animFrames),(float)textures::player.height};
			jumping = false;
			groundPosition = { 0,0 };
		}

		void Player::move()
		{
			if (jumping)
			{
				if (!going_down)
				{
					hitbox.y -= GetFrameTime() * 150;
					if (hitbox.y < target.y)
					{
						hitbox.y = target.y;
						target.y = groundPosition.y;
						going_down = true;
					}
				}
				else
				{
					hitbox.y += GetFrameTime() * 150;
					if (hitbox.y > target.y)
					{
						hitbox.y = target.y;
						going_down = false;
						jumping = false;
					}
				}
			}
			else
			{
				sourceRec.x += (float)textures::player.width / animFrames;
				if (sourceRec.x > textures::player.width)
				{
					sourceRec.x = 0;
				}


				if (IsKeyPressed(duck))
				{
					if (_current == state::down)
					{
						_current = state::middleLane;
					}
					else if (_current == state::middleLane)
					{
						_current = state::topLane;
					}
				}
				if (IsKeyPressed(KEY_W))
				{
					if (_current == state::topLane)
					{
						_current = state::middleLane;
					}
					else if (_current == state::middleLane)
					{
						_current = state::down;
					}
				}
				if (IsKeyPressed(phase))
				{
					if (!_phasing && !_flaming)
					{
						_phasing = true;
					}
				}
				if (IsKeyPressed(flame))
				{
					if (!_phasing && !_flaming)
					{
						_flaming = true;
					}
				}
				if (IsKeyPressed(go_up))
				{
					target.y = hitbox.y - (hitbox.height * 1.5);
					groundPosition.x = hitbox.x;
					groundPosition.y = hitbox.y;
					jumping = true;
					going_down = false;
				}

				switch (_current)
				{
				case state::down:
					hitbox.y = screen::topLane.y - hitbox.height;
					break;
				case state::middleLane:
					hitbox.y = screen::middleLane.y - hitbox.height;
					break;
				case state::topLane:
					hitbox.y = screen::Lane.y - hitbox.height;
					break;
				}
			}
		}

		void Player::score()
		{
			_score++;
		}

		Rectangle Player::getHitBox()
		{
			return hitbox;
		}

		void Player::draw()
		{
			DrawTexturePro(textures::player, sourceRec, hitbox, { 0,0 }, 0, WHITE);

			if (_phasing)
			{
				DrawTexturePro(textures::player, sourceRec, hitbox, {0,0}, 0, BLUE);
			}
			if(_flaming)
			{
				DrawTexturePro(textures::player, sourceRec, hitbox, { 0,0 }, 0, RED);
			}
		}

		bool Player::getPhase()
		{
			return _phasing;
		}

		void Player::phaseChange()
		{
			_counter++;

			if (_counter == 60)
			{
				_seconds++;
				_counter = 0;
				if (_seconds == 2)
				{
					_phasing = false;
					_seconds = 0;
					_counter = 0;
				}
			}
		}

		void Player::flameChange()
		{
			_counter++;

			if (_counter == 60)
			{
				_seconds++;
				_counter = 0;
				if (_seconds == 2)
				{
					_flaming = false;
					_seconds = 0;
					_counter = 0;
				}
			}
		}

		float Player::getX()
		{
			return (hitbox.x + hitbox.width);
		}

		void Player::loseLives()
		{
			_lives--;
			if (_lives <= 0)
			{
				_gameover = true;
			}
		}

		bool Player::getGameOver()
		{
			return _gameover;
		}

		state Player::getCurrent()
		{
			return _current;
		}

		float Player::getY()
		{
			return (hitbox.y + hitbox.height);
		}

		int Player::getlives()
		{
			return _lives;
		}

		bool Player::getFlaming()
		{
			return _flaming;
		}

		short Player::getSeconds()
		{
			return _score;
		}

		void Player::jump()
		{

		}
	}
}