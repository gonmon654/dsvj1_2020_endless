#ifndef PAUSE_H
#define PAUSE_H
#include "raylib.h"
#include "Run/Run.h"
namespace endless
{
	namespace pause
	{
		extern Rectangle hitbox[2];
		void init();
		void draw();
		void update();
	}
}
#endif // !PAUSE_H

