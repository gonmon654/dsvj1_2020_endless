#ifndef  TEXTURES_H
#define TEXTURES_H
#include "raylib.h"
#include "Screen Measures/Screen Size and Adjustments.h"
#include "Objects/Obstacle/Obstacle.h"
#include "Objects/Player/Player.h"
namespace endless
{
	namespace textures
	{
		//Background Texture 
		extern Texture2D background;

		//Gameplay Textures
		extern Texture2D gamePlayBackgrounds[4];
		
		//Main Menu Textures
		extern Texture2D mainMenuButtons[3];

		//Settings Textures
		extern Texture2D back;
		extern Texture2D audio;
		extern Texture2D credits;

		//Player Textures
		extern Texture2D player;
		extern Texture2D monster;

		//Credits Textures
		extern Texture2D backToSettings;
		extern Texture2D Credits;

		//Pause Textures
		extern Texture2D resume;
		extern Texture2D mainMenu;

		//Loading textures for specific parts
		void loadTexturesMainMenu();
		void loadTexturesPlay();
		void loadTexturesSettings();
		void loadTexturesCredits();
		void loadBackground();

		//Unloading textures for specific parts
		void unLoadTexturesMainMenu();
		void unLoadTexturesSettings();
		void unLoadTexturesPlay();
		void unLoadTexturesCredits();
		void unLoadBackground();
	}
	namespace sounds
	{
		extern Music mainMenuMusic;
		extern Music gameplayMusic;
		extern Sound damageTaken;
		extern Sound menuMove;
		extern Sound menuSelect;

		void initSounds();
		void deInitSounds();
	}
}

#endif // ! TEXTURES_H

