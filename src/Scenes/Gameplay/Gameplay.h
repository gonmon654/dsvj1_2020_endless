#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"

#include "Run/Run.h"

#include "Objects/Player/Player.h"
#include "Objects/Obstacle/Obstacle.h"
#include "Textures/Textures.h"

namespace endless
{
	namespace gameplay
	{
		extern int highScore;
		extern bool gameOver;
		extern player::Player* player;
		extern obstacle::Obstacle* obstacle;
		struct positions
		{
			Vector2 positionBackground[3] = { { 0,0 }, { 0,0 }, { 0,0 } };
			float modifier = 20.0f;
		};
		extern positions backgrounds[4];

		void init();
		void update();
		void draw();
		void deInit();
		void moveParallax();
		void drawParallax();
	}
}
#endif // !GAMEPLAY_H

