#include "Main Menu.h"
#include "Scenes/Gameplay/Gameplay.h"

namespace endless
{
	namespace mainMenu
	{
		bool selected = false;
		Rectangle playLocation;
		Rectangle settingsLocation;
		Rectangle exitLocation;

		void init()
		{

			playLocation = { (float)((screen::screenWidth / 2)),(float)((screen::screenHeight / 8) * 2), (float)((screen::screenWidth / 4) * 1) ,(float)((screen::screenHeight / 8) * 1) };
			settingsLocation = { (float)((screen::screenWidth / 2)),(float)((screen::screenHeight / 8) * 4),  (float)((screen::screenWidth / 4) * 1),(float)((screen::screenHeight / 8) * 1) };
			exitLocation = { (float)((screen::screenWidth / 2)),(float)((screen::screenHeight / 8) * 6), (float)((screen::screenWidth / 4) * 1),(float)((screen::screenHeight / 8) * 1) };

			textures::loadTexturesMainMenu();

			textures::mainMenuButtons[0].width = (int)playLocation.width;
			textures::mainMenuButtons[0].height = (int)playLocation.height;

			textures::mainMenuButtons[1].width = (int)settingsLocation.width;
			textures::mainMenuButtons[1].height = (int)settingsLocation.height;

			textures::mainMenuButtons[2].width = (int)exitLocation.width;
			textures::mainMenuButtons[2].height = (int)exitLocation.height;
		}

		void draw()
		{
			DrawTexture(textures::background, 0, 0, WHITE);
			DrawText(FormatText("HighScore: %01i", gameplay::highScore), (int)((screen::screenWidth / 4) * 1), (int)(screen::screenHeight / 4), 20, BLACK);
			DrawTexture(textures::mainMenuButtons[0], (int)playLocation.x, (int)playLocation.y, WHITE);
			DrawTexture(textures::mainMenuButtons[1], (int)settingsLocation.x, (int)settingsLocation.y, WHITE);
			DrawTexture(textures::mainMenuButtons[2], (int)exitLocation.x, (int)exitLocation.y, WHITE);
		}

		void update()
		{
			if (CheckCollisionPointRec(GetMousePosition(), playLocation))
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					if (selected != true)
					{
						PlaySound(sounds::menuSelect);
						selected = true;
					}
					run::newStatus = run::gamestate::PLAY;
					run::changeStatus();
					return;
				}
			}

			else if (CheckCollisionPointRec(GetMousePosition(), settingsLocation))
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					if (selected != true)
					{
						PlaySound(sounds::menuSelect);
						selected = true;
					}
					run::newStatus = run::gamestate::SETTINGS;
					run::changeStatus();
					return;
				}
			}

			else if (CheckCollisionPointRec(GetMousePosition(), exitLocation))
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					if (selected != true)
					{
						PlaySound(sounds::menuSelect);
						selected = true;
					}
					run::newStatus = run::gamestate::EXIT;
					run::changeStatus();
					return;
				}
			}

			else 
			{
				selected=false;
				return;
			}
		}
	}
}