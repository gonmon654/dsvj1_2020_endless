#ifndef OBSTACLE_H
#define OBSTACLE_H

#include "raylib.h"

#include "Screen Measures/Screen Size and Adjustments.h"
#include "Textures/Textures.h"

namespace endless
{
	namespace obstacle
	{
		enum class type { topLane = 1, middleLane, down, full};
		class Obstacle
		{
		private:
			float _obstacleSpeed;
			bool _killable;
			Rectangle _hitbox;
			Rectangle sourceRec;
			type _state;
		public:
			Obstacle();
			void init();
			void update();
			float getX();
			void draw();
			type getCurrent();
			float getY();
			bool getKillable();
			Rectangle getHitbox();
		};
	}
}
#endif // !OBSTACLE_H

