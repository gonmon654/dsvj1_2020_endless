#include "Credits.h"
namespace endless
{
	namespace credits
	{
		Rectangle back;
		void init()
		{
			back = { (float)((screen::screenWidth / 4)),(float)((screen::screenHeight / 8) * 6), (float)((screen::screenWidth / 4) * 1),(float)((screen::screenHeight / 8) * 1) };
			textures::backToSettings.width = (int)back.width;
			textures::backToSettings.height = (int)back.height;

			textures::Credits.width = screen::screenWidth;
			textures::Credits.height = screen::screenHeight;
		}
		void draw()
		{
			DrawTexture(textures::Credits, 0, 0, WHITE);
			DrawTexture(textures::backToSettings, (int)back.x, (int)back.y, WHITE);
		}
		void update()
		{
			if (CheckCollisionPointRec(GetMousePosition(), back))
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					PlaySound(sounds::menuSelect);
					run::newStatus = run::gamestate::SETTINGS;
					run::changeStatus();
				}
			}
		}
	}
}
