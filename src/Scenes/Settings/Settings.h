#ifndef SETTINGS_H
#define SETTINGS_H

#include "raylib.h"

#include "Run/Run.h"
#include "Textures/Textures.h"

namespace endless
{
	namespace settings
	{
		enum class state { CONTROLS, SCREEN, BACK, NONE };
		extern bool _audioOn;
		extern bool selected;
		extern state current;

		extern Rectangle controls;
		extern Rectangle screenSize;
		extern Rectangle back;

		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !SETTINGS_H

