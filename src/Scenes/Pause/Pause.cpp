#include "Pause.h"
namespace endless
{
	namespace pause
	{
		Rectangle hitbox[2];
		void init()
		{
			hitbox[0] = { (float)((screen::screenWidth / 2)),(float)((screen::screenHeight / 8) * 2), (float)((screen::screenWidth / 4) * 1) ,(float)((screen::screenHeight / 8) * 1) };
			hitbox[1] = { (float)((screen::screenWidth / 2)),(float)((screen::screenHeight / 8) * 4),  (float)((screen::screenWidth / 4) * 1),(float)((screen::screenHeight / 8) * 1) };

			textures::resume.width = (int)hitbox[0].width;
			textures::resume.height = (int)hitbox[0].height;

			textures::mainMenu.width = (int)hitbox[1].width;
			textures::mainMenu.height = (int)hitbox[1].height;
		}
		void draw()
		{
			DrawTexture(textures::resume, (int)hitbox[0].x, (int)hitbox[0].y, WHITE);
			DrawTexture(textures::mainMenu, (int)hitbox[1].x, (int)hitbox[1].y, WHITE);
		}
		void update()
		{
			if (CheckCollisionPointRec(GetMousePosition(), hitbox[0]))
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					run::newStatus = run::gamestate::PLAY;
					run::changeStatus();
				}
			}
			if (CheckCollisionPointRec(GetMousePosition(), hitbox[1]))
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					run::newStatus = run::gamestate::MAINMENU;
					run::changeStatus();
				}
			}
			
		}
	}
}