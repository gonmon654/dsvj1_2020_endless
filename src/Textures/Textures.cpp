#include "Textures.h"

namespace endless
{
	namespace textures
	{
		//Background Textures
		Texture2D background;

		//Gameplay Textures
		Texture2D gamePlayBackgrounds[4];

		//Main Menu Textures
		Texture2D mainMenuButtons[3];

		//Credits Textures
		Texture2D backToSettings;
		Texture2D Credits;

		//Settings Textures
		Texture2D audio;
		Texture2D credits;
		Texture2D back;

		//Character Textures
		Texture2D player;
		Texture2D monster;

		//Pause Textures
		Texture2D resume;
		Texture2D mainMenu;

		void loadBackground()
		{
			background = LoadTexture("Resources/Assets/Background 0 Gameplay.png");
			background.height = screen::screenHeight;
			background.width = screen::screenWidth;
		}

		void loadTexturesMainMenu()
		{
			mainMenuButtons[0] = LoadTexture("Resources/Assets/Play.png");

			mainMenuButtons[1] = LoadTexture("Resources/Assets/Settings.png");

			mainMenuButtons[2] = LoadTexture("Resources/Assets/Quit.png");

		}

		void loadTexturesPlay()
		{
			gamePlayBackgrounds[0] = LoadTexture("Resources/Assets/Background 0 Gameplay.png");
			gamePlayBackgrounds[1] = LoadTexture("Resources/Assets/Background 3 Gameplay.png");
			gamePlayBackgrounds[2] = LoadTexture("Resources/Assets/Background 2 Gameplay.png");
			gamePlayBackgrounds[3] = LoadTexture("Resources/Assets/Background 1 Gameplay.png");
			player = LoadTexture("Resources/Assets/Character Sheet.png");
			monster = LoadTexture("Resources/Assets/Monster Sheet.png");
			resume = LoadTexture("Resources/Assets/Resume.png");
			mainMenu = LoadTexture("Resources/Assets/Main Menu.png");
		}

		void loadTexturesSettings()
		{
			audio = LoadTexture("Resources/Assets/Sounds.png");
			credits = LoadTexture("Resources/Assets/Credits.png");
			back = LoadTexture("Resources/Assets/Quit.png");
		}

		void loadTexturesCredits()
		{
			backToSettings = LoadTexture("Resources/Assets/Quit.png");
			Credits = LoadTexture("Resources/Assets/Credits Image.png");
		}

		void unLoadBackground()
		{
			UnloadTexture(background);
		}

		void unLoadTexturesMainMenu()
		{
			for (short i = 0; i < 3; i++)
			{
				UnloadTexture(mainMenuButtons[i]);
			}
		}

		void unLoadTexturesSettings()
		{

		}

		void unLoadTexturesPlay()
		{
			for (short i = 0; i < 4; i++)
			{
				UnloadTexture(gamePlayBackgrounds[i]);
			}
			UnloadTexture(player);
			UnloadTexture(monster);
		}

		void unLoadTexturesCredits()
		{
			UnloadTexture(backToSettings);
			UnloadTexture(Credits);
		}
	}
	namespace sounds
	{
		Music mainMenuMusic;
		Music gameplayMusic;

		Sound damageTaken;
		Sound menuMove;
		Sound menuSelect;

		void initSounds()
		{
			InitAudioDevice();

			mainMenuMusic = LoadMusicStream("Resources/Assets/Main Menu Music.mp3");
			gameplayMusic = LoadMusicStream("Resources/Assets/Gameplay Music.mp3");

			SetMusicVolume(mainMenuMusic, 0.5f);
			SetMusicVolume(gameplayMusic, 0.5f);

			damageTaken = LoadSound("Resources/Assets/Damage Taken.wav");
			menuMove = LoadSound("Resources/Assets/Menu Move.wav");
			menuSelect = LoadSound("Resources/Assets/Menu Move.wav");

			SetSoundVolume(damageTaken, 0.5f);
			SetSoundVolume(menuMove, 0.5f);
			SetSoundVolume(menuSelect, 0.5f);
		}
		void deInitSounds()
		{
			UnloadMusicStream(mainMenuMusic);
			UnloadMusicStream(gameplayMusic);

			UnloadSound(damageTaken);
			UnloadSound(menuMove);
			UnloadSound(menuSelect);
		}
	}
}