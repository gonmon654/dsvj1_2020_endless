#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "raylib.h"

#include "Screen Measures/Screen Size and Adjustments.h"
#include "Textures/Textures.h"

#include "Run/Run.h"

namespace endless
{
	namespace mainMenu
	{
		void init();
		void draw();
		void update();

		extern Rectangle playLocation;
		extern Rectangle settingsLocation;
		extern Rectangle exitLocation;
		extern bool selected;
	}
}
#endif // !MAIN_MENU_H

