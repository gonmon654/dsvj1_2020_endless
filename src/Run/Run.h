#ifndef RUN_H
#define RUN_H

#include "raylib.h"

#include "Screen Measures/Screen Size and Adjustments.h"
#include "Textures/Textures.h"

#include "Scenes/Gameplay/Gameplay.h"
#include "Scenes/Main Menu/Main Menu.h"
#include "Scenes/Settings/Settings.h"
#include "Scenes/Credits/Credits.h"
#include "Scenes/Pause/Pause.h"


namespace endless
{
	namespace run
	{
		enum class gamestate {PLAY=1,MAINMENU,SETTINGS,EXIT,CREDITS,PAUSE};
		extern gamestate status;
		extern gamestate newStatus;

		void init();
		void update();
		void draw();
		void deInit();
		void run();
		void changeStatus();
	}
}
#endif // !RUN_H

