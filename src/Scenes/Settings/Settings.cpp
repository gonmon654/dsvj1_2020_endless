#include "Settings.h"

namespace endless
{
	namespace settings
	{
		bool selected = false;
		bool _audioOn = true;
		Rectangle controls;
		Rectangle screenSize;
		Rectangle back;

		state current;

		void init()
		{
			textures::loadTexturesSettings();

			current = state::NONE;

			controls = { (float)((screen::screenWidth / 2)),(float)((screen::screenHeight / 8) * 2), (float)((screen::screenWidth / 4) * 1) ,(float)((screen::screenHeight / 8) * 1) };
			screenSize = { (float)((screen::screenWidth / 2)),(float)((screen::screenHeight / 8) * 4),  (float)((screen::screenWidth / 4) * 1),(float)((screen::screenHeight / 8) * 1) };
			back = { (float)((screen::screenWidth / 2)),(float)((screen::screenHeight / 8) * 6), (float)((screen::screenWidth / 4) * 1),(float)((screen::screenHeight / 8) * 1) };

			textures::credits.height = (int)controls.height;
			textures::credits.width = (int)controls.width;

			textures::audio.height = (int)screenSize.height;
			textures::audio.width = (int)screenSize.width;
			
			textures::back.height = (int)back.height;
			textures::back.width = (int)back.width;
		}

		void update()
		{
			if (CheckCollisionPointRec(GetMousePosition(), controls))
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					if (_audioOn)
					{
						if (selected != true)
						{
							PlaySound(sounds::menuSelect);
							selected = true;
						}
						_audioOn = false;
						SetMusicVolume(sounds::mainMenuMusic, 0.0f);
						SetMusicVolume(sounds::gameplayMusic, 0.0f);
						SetSoundVolume(sounds::damageTaken, 0.0f);
						SetSoundVolume(sounds::menuMove, 0.0f);
						SetSoundVolume(sounds::menuSelect, 0.0f);
					}
					else
					{
						_audioOn = true;
						SetMusicVolume(sounds::mainMenuMusic, 0.5f);
						SetMusicVolume(sounds::gameplayMusic, 0.5f);
						SetSoundVolume(sounds::damageTaken, 0.5f);
						SetSoundVolume(sounds::menuMove, 0.5f);
						SetSoundVolume(sounds::menuSelect, 0.5f);
					}
				}
			}

			else if (CheckCollisionPointRec(GetMousePosition(), screenSize))
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					if (selected != true)
					{
						PlaySound(sounds::menuSelect);
						selected = true;
					}
					run::newStatus = run::gamestate::CREDITS;
					run::changeStatus();
				}
			}

			else if (CheckCollisionPointRec(GetMousePosition(), back))
			{
				if (selected != true)
				{
					PlaySound(sounds::menuSelect);
					selected = true;
				}
				current = state::BACK;
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					run::newStatus = run::gamestate::MAINMENU;
					run::changeStatus();
				}
			}

			else
			{
				current = state::NONE;
			}
		}

		void draw()
		{
			DrawTexture(textures::background, 0, 0, WHITE);
			DrawTexture(textures::audio, (int)controls.x, (int)controls.y, WHITE);
			DrawTexture(textures::credits, (int)screenSize.x, (int)screenSize.y, WHITE);
			DrawTexture(textures::back, (int)back.x, (int)back.y, WHITE);
		}

		void deInit()
		{
			textures::unLoadTexturesSettings();
		}
	}
}