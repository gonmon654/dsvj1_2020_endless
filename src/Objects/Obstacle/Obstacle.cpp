#include "Obstacle.h"

namespace endless
{
	namespace obstacle
	{
		const int animFrames = 14;

		Obstacle::Obstacle()
		{
			init();
		}

		void Obstacle::init()
		{
			sourceRec = { 0,0,(float)(textures::monster.width / animFrames),(float)textures::monster.height };

			float height = 0.0;
			switch (GetRandomValue(1, 4))
			{
			case 1:
				_state = type::topLane;
				height = screen::topLane.y;
				switch (GetRandomValue(1, 2))
				{
				case 1:
					_killable = true;
					break;
				case 2:
					_killable = false;
					break;
				}
				break;
			case 2:
				_state = type::middleLane;
				height = (screen::middleLane.y) - (screen::topLane.y + screen::topLane.height);
				switch (GetRandomValue(1, 2))
				{
				case 1:
					_killable = true;
					break;
				case 2:
					_killable = false;
					break;
				}
				break;
			case 3:
				_state = type::down;
				height = (screen::Lane.y) - (screen::middleLane.y + screen::middleLane.height);
				switch (GetRandomValue(1, 2))
				{
				case 1:
					_killable = true;
					break;
				case 2:
					_killable = false;
					break;
				}
				break;
			case 4:
				_state = type::full;
				height = (screen::Lane.y);
				_killable = false;
				break;
			}

			switch (_state)
			{
			case type::topLane:
				_hitbox = { (float)(screen::screenWidth),(float)(0),(float)((screen::screenWidth / 20) * 1),(float)(height) };
				if (!_killable)
				{
					switch (GetRandomValue(1, 2))
					{
					case 1:
						_hitbox.height = (_hitbox.height / 2);
						_hitbox.y += _hitbox.height;
						break;
					case 2:
						break;
					}
				}
				break;
			case type::middleLane:
				_hitbox = { (float)(screen::screenWidth),(float)(screen::topLane.y + screen::topLane.height),(float)((screen::screenWidth / 20) * 1),(float)(height) };
				if (!_killable)
				{
					switch (GetRandomValue(1, 2))
					{
					case 1:
						_hitbox.height = (_hitbox.height / 2);
						_hitbox.y += _hitbox.height;
						break;
					case 2:
						break;
					}
				}
				break;
			case type::down:
				_hitbox = { (float)(screen::screenWidth),(float)(screen::middleLane.y + screen::middleLane.height),(float)((screen::screenWidth / 20) * 1),(float)(height) };
				if (!_killable)
				{
					switch (GetRandomValue(1, 2))
					{
					case 1:
						_hitbox.height = (_hitbox.height / 2);
						_hitbox.y += _hitbox.height;
						break;
					case 2:
						break;
					}
				}
				break;
			case type::full:
				_hitbox = { (float)(screen::screenWidth),(float)(0),(float)(screen::screenWidth / 20) * 3,(float)(height) };
				break;
			}

			_obstacleSpeed = 200.0f;
			
		}

		void Obstacle::update()
		{
			sourceRec.x += (float)textures::monster.width / animFrames;
			if (sourceRec.x > textures::monster.width)
			{
				sourceRec.x = 0;
			}
			_hitbox.x -= _obstacleSpeed * GetFrameTime();
		}

		float Obstacle::getX()
		{
			return _hitbox.x;
		}

		void Obstacle::draw()
		{
			if (_killable)
			{
				DrawTexturePro(textures::monster, sourceRec, _hitbox, { 0,0 }, 0, BLUE);
			}
			else
			{
				DrawTexturePro(textures::monster, sourceRec, _hitbox, { 0,0 }, 0, WHITE);
			}		
		}

		type Obstacle::getCurrent()
		{
			return _state;
		}

		float Obstacle::getY()
		{
			return (_hitbox.y + _hitbox.height);
		}

		bool Obstacle::getKillable()
		{
			return _killable;
		}

		Rectangle Obstacle::getHitbox()
		{
			return _hitbox;
		}
	}
}