#ifndef CREDITS_H
#define CREDITS_H
#include "raylib.h"
#include "Run/Run.h"
namespace endless
{
	namespace credits
	{
		extern Rectangle back;
		void init();
		void draw();
		void update();
	}
}
#endif // !CREDITS_H

