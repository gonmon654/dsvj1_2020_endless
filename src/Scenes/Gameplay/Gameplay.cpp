#include "Gameplay.h"

namespace endless
{
	namespace gameplay
	{
		int highScore;
		bool gameOver;
		player::Player* player;
		obstacle::Obstacle* obstacle;
		positions backgrounds[4];

		void checkObjectPosition()
		{
			if (obstacle->getX() < 0)
			{
				if (!obstacle->getKillable())
				{
					player->score();
				}
				delete obstacle;
				obstacle = new obstacle::Obstacle();
			}
		}

		void checkCollision()
		{
			bool collision = CheckCollisionRecs(player->getHitBox(), obstacle->getHitbox());
			if (!player->getPhase() && !player->getFlaming())
			{
				if (collision)
				{
					player->loseLives();
					PlaySound(sounds::damageTaken);

					if (player->getGameOver())
					{
						gameOver = true;
						if (gameOver == true)
						{
							if (highScore < player->getSeconds())
							{
								highScore = player->getSeconds();
							}
							run::newStatus = run::gamestate::MAINMENU;
							run::changeStatus();
						}
					}
					delete obstacle;
					obstacle = new obstacle::Obstacle();
					return;
				}
			}
			if (player->getFlaming())
			{
				
				if (collision)
				{
					if (obstacle->getKillable())
					{
						player->score();
						delete obstacle;
						obstacle = new obstacle::Obstacle();
					}
					else
					{
						player->loseLives();
						PlaySound(sounds::damageTaken);

						if (player->getGameOver())
						{
							gameOver = true;
							if (gameOver == true)
							{
								if (highScore < player->getSeconds())
								{
									highScore = player->getSeconds();
								}
								run::newStatus = run::gamestate::MAINMENU;
								run::changeStatus();
							}
						}
						delete obstacle;
						obstacle = new obstacle::Obstacle();
						return;
					}
				}
			}
		}

		bool matchlanes()
		{
			if (player->getCurrent() == player::state::topLane)
			{
				if (obstacle->getCurrent() == obstacle::type::down || obstacle->getCurrent() == obstacle::type::full)
				{
					return true;
				}
			}
			if (player->getCurrent() == player::state::middleLane)
			{
				if (obstacle->getCurrent() == obstacle::type::middleLane || obstacle->getCurrent() == obstacle::type::full)
				{
					return true;
				}
			}
			if (player->getCurrent() == player::state::down)
			{
				if (obstacle->getCurrent() == obstacle::type::topLane || obstacle->getCurrent() == obstacle::type::full)
				{
					return true;
				}
			}
			return false;
		}

		void drawLanes()
		{
			DrawRectangleRec(screen::Lane, BLACK);
			DrawRectangleRec(screen::middleLane, BLACK);
			DrawRectangleRec(screen::topLane, BLACK);
			DrawRectangleRec(screen::gameData, BLACK);		
			DrawText(FormatText("Lives: %01i", player->getlives()), (int)(screen::gameData.x + (screen::gameData.width / 4)), (int)(screen::gameData.y + ((screen::gameData.height / 4) * 1)), 20, WHITE);
			DrawText(FormatText("Score: %01i", player->getSeconds()), (int)(screen::gameData.x + (screen::gameData.width / 4)), (int)(screen::gameData.y + ((screen::gameData.height / 4) * 2)), 20, WHITE);
		}

		void init()
		{
			textures::loadTexturesPlay();
			player = new player::Player();
			obstacle = new obstacle::Obstacle();
			gameOver = false;
			for (short i = 0; i < 4; i++)
			{
				backgrounds[i].positionBackground[0] = { (float)-screen::screenWidth,0 };
				backgrounds[i].positionBackground[1] = { 0,0 };
				backgrounds[i].positionBackground[2] = { (float)screen::screenWidth,0 };
				backgrounds[i].modifier *= i + 1;
			}
		}

		void update()
		{
			player->move();
			obstacle->update();
			moveParallax();
			if (player->getPhase())
			{
				player->phaseChange();
			}
			if (player->getFlaming())
			{
				player->flameChange();
			}

			if (matchlanes())
			{
				checkCollision();
			}

			if (IsKeyPressed(KEY_E))
			{
				if (highScore < player->getSeconds())
				{
					highScore = player->getSeconds();
				}
				run::newStatus = run::gamestate::PAUSE;
				run::changeStatus();
			}

			checkObjectPosition();
		}

		void draw()
		{	
			drawParallax();
			drawLanes();
			obstacle->draw();
			player->draw();
		}

		void deInit()
		{
			delete player;
			delete obstacle;
		}

		void moveParallax()
		{
			for (short i = 0; i < 4; i++)
			{
				for (short j = 0; j < 3; j++)
				{
					backgrounds[i].positionBackground[j].x -= GetFrameTime() * backgrounds[i].modifier;
					if (backgrounds[i].positionBackground[j].x < -screen::screenWidth)
					{
						backgrounds[i].positionBackground[j].x = (float)screen::screenWidth - 1;
					}
				}
			}
		}

		void drawParallax()
		{
			for (short i = 0; i < 4; i++)
			{
				for (short j = 0; j < 3; j++)
				{
					Rectangle _sourcerec = { 0,0,(float)textures::gamePlayBackgrounds[i].width,(float)textures::gamePlayBackgrounds[i].height };
					Rectangle _desrec = { (float)backgrounds[i].positionBackground[j].x,0,(float)screen::screenWidth,(float)screen::screenHeight - screen::gameData.height };
					DrawTexturePro(textures::gamePlayBackgrounds[i], _sourcerec, _desrec, { 0,0 }, 0, WHITE);
				}
			}
		}
	}
}