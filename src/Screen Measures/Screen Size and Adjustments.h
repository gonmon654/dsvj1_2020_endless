#ifndef SCREEN_SIZE_AND_ADJUSTMENTS_H
#define SCREEN_SIZE_AND_ADJUSTMENTS_H

#include "raylib.h"

namespace endless
{
	namespace screen
	{
		extern int screenWidth;
		extern int screenHeight;

		extern Rectangle topLane;
		extern Rectangle middleLane;
		extern Rectangle Lane;
		extern Rectangle gameData;

		void initLanes();
	}
}

#endif // !SCREEN_SIZE_AND_ADJUSTMENTS_H
