#include "Screen Size and Adjustments.h"

namespace endless
{
	namespace screen
	{
		int screenHeight = 450;
		int screenWidth = 800;
		
		Rectangle topLane;
		Rectangle middleLane;
		Rectangle Lane;
		Rectangle gameData;

		void initLanes()
		{
			topLane = { 0, (float)((screenHeight / 8) * 2), (float)(screenWidth),(float)((screenHeight / 16) * 1) };
			middleLane = { 0,(float)((screenHeight / 8) * 4),(float)(screenWidth),(float)((screenHeight / 16) * 1) };
			Lane = { 0,(float)((screenHeight / 8) * 6),(float)(screenWidth),(float)((screenHeight / 16) * 1) };
			gameData = { 0,(float)(screen::Lane.y + screen::Lane.height),(float)(screenWidth),(float)(screenHeight - (screen::Lane.y + screen::Lane.height)) };
		}
	}
}