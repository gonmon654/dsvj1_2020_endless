#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"
#include "Screen Measures/Screen Size and Adjustments.h"
#include "Textures/Textures.h"

namespace endless
{
	namespace player
	{
		enum class state { topLane = 1, middleLane, down};
		extern int playerHeight;
		extern int playerWidth;

		extern int jump;
		extern int duck;
		extern int phase;

		class Player
		{
		private:
			short _lives;
			bool _phasing;
			bool _flaming;
			short _counter;
			short _seconds;
			bool _gameover;
			short _score;
			bool jumping;
			bool going_down;
			state _current;
			Rectangle hitbox;
			Rectangle sourceRec;
			Vector2 target;
			Vector2 groundPosition;
		public:
			Player();
			void move();
			void init();
			void score();
			Rectangle getHitBox();
			void draw();
			bool getPhase();
			void phaseChange();
			float getX();
			void loseLives();
			bool getGameOver();
			state getCurrent();
			float getY();
			int getlives();
			void flameChange();
			bool getFlaming();
			short getSeconds();
			void jump();
		};
	}
}



#endif // !PLAYER_H
