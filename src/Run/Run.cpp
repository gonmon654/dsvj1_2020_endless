#include "Run.h"

namespace endless
{
	namespace run
	{
		gamestate status;
		gamestate newStatus;

		void init()
		{
			InitWindow(screen::screenWidth, screen::screenHeight, "Endless - By Gonza Montero");

			SetTargetFPS(60);
			textures::loadBackground();
			mainMenu::init();
		}

		void update()
		{
			switch (status)
			{
			case gamestate::PLAY:
				if (IsMusicPlaying(sounds::gameplayMusic))
				{
					UpdateMusicStream(sounds::gameplayMusic);
				}
				else
				{
					PlayMusicStream(sounds::gameplayMusic);
				}
				gameplay::update();
				break;
			case gamestate::MAINMENU:
				if (IsMusicPlaying(sounds::mainMenuMusic))
				{
					UpdateMusicStream(sounds::mainMenuMusic);
				}
				else
				{
					PlayMusicStream(sounds::mainMenuMusic);
				}
				mainMenu::update();
				break;
			case gamestate::SETTINGS:
				if (IsMusicPlaying(sounds::mainMenuMusic))
				{
					UpdateMusicStream(sounds::mainMenuMusic);
				}
				else
				{
					PlayMusicStream(sounds::mainMenuMusic);
				}
				settings::update();
				break;
			case gamestate::CREDITS:
				if (IsMusicPlaying(sounds::mainMenuMusic))
				{
					UpdateMusicStream(sounds::mainMenuMusic);
				}
				else
				{
					PlayMusicStream(sounds::mainMenuMusic);
				}
				credits::update();
				break;
			case gamestate::PAUSE:
				pause::update();
				break;
			}
		}

		void draw()
		{
			BeginDrawing();
			ClearBackground(WHITE);

			switch (status)
			{
			case gamestate::PLAY:
				gameplay::draw();
				break;
			case gamestate::MAINMENU:
				mainMenu::draw();
				break;
			case gamestate::SETTINGS:
				settings::draw();
				break;
			case gamestate::CREDITS:
				credits::draw();
				break;
			case gamestate::PAUSE:
				pause::draw();
			}
			EndDrawing();
		}

		void deInit()
		{
			textures::unLoadTexturesMainMenu();
		}

		void run()
		{
			status = gamestate::MAINMENU;

			sounds::initSounds();

			init();

			while (status != gamestate::EXIT && !WindowShouldClose())
			{
				update();
				draw();
			}

			deInit();
			CloseWindow();
			sounds::deInitSounds();
		}

		void changeStatus()
		{
			bool arrivedFromPause = false;
			switch (newStatus)
			{
			case gamestate::PLAY:
				if (status == gamestate::PAUSE)
				{
					arrivedFromPause = true;
				}
				textures::unLoadBackground();
				textures::unLoadTexturesMainMenu();
				status = gamestate::PLAY;
				break;
			case gamestate::MAINMENU:
				if (status == gamestate::PLAY)
				{
					textures::unLoadTexturesPlay();
				}
				if (status == gamestate::SETTINGS)
				{
					textures::unLoadTexturesSettings();
				}
				status = gamestate::MAINMENU;
				break;
			case gamestate::SETTINGS:
				textures::unLoadTexturesMainMenu();
				status = gamestate::SETTINGS;
				break;
			case gamestate::CREDITS:
				status = gamestate::CREDITS;
				break;
			case gamestate::EXIT:
				status = gamestate::EXIT;
				break;
			case gamestate::PAUSE:
				status = gamestate::PAUSE;
				break;
			}

			switch (status)
			{
			case gamestate::PLAY:
				if (!arrivedFromPause)
				{
					screen::initLanes();
					gameplay::init();
				}
				textures::loadTexturesPlay();
				break;
			case gamestate::MAINMENU:
				textures::loadBackground();
				mainMenu::init();
				break;
			case gamestate::SETTINGS:
				settings::init();
				break;
			case gamestate::CREDITS:
				textures::loadTexturesCredits();
				credits::init();
				break;
			case gamestate::PAUSE:
				pause::init();
				break;
			}
		}
	}
}